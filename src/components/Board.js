import React from 'react';

import Square from 'components/Square';

class Board extends React.Component {

  renderSquare(i) {

    const winners = this.props.winningSquares;
    const displayClass = (winners && winners.includes(i)) ? 'square winning-square' : 'square';

    return (
      <Square
        key={`square${i}`}
        className={displayClass}
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  renderGrid(columnCount, rowCount) {
    let rows = [];
    let squares = [];
    let index = 0;

    for (let i = 0; i < rowCount; i++) {
      for (let j = 0; j < columnCount; j++) {
        squares.push(this.renderSquare(index))
        index++;
      }
      rows.push(<div key={`row${i}`} className="board-row">{squares}</div>)
      squares = [];
    }

    return rows;
  }

  render() {
    return (
      <div>
        {this.renderGrid(3, 3)}
      </div>
    );
  }
}

export default Board;