import React from 'react';

class Sort extends React.Component {
  render() {

    let direction = this.props.ascending ? 'Ascending' : 'Descending';
    let message = `Sort ${direction}`;

    return (
      <button onClick={() => this.props.onClick()}>{message}</button>
    );
  }
}

export default Sort;