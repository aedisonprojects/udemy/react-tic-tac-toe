import React from 'react';

import calculateWinner from 'utilities/victory.js';
import Board from 'components/Board';
import Sort from 'components/Sort';

class Game extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        turn: null,
        location: -1
      }],
      turnX: true,
      viewStep: 0,
      ascending: true
    };
  }

  fillSquare(i) {
    const currentHistoryIndex = this.state.viewStep + 1;
    const history = this.state.history.slice(0, currentHistoryIndex);

    const lastIndexInHistory = history.length - 1;
    const current = history[lastIndexInHistory];
    const squares = current.squares.slice();

    const winner = calculateWinner(squares);

    if (squares[i] || winner) {
      return;
    }

    squares[i] = this.state.turnX ? 'X' : 'O';
    const nextTurn = !this.state.turnX;

    this.setState({
      history: history.concat([{
        squares: squares,
        turn: squares[i],
        location: i,
      }]),
      viewStep: history.length,
      turnX: nextTurn
    });
  }

  jumpTo(step) {
    this.setState({
      viewStep: step,
      turnX: (step % 2) === 0,
    });
  }

  sortHistory() {
    this.setState({
      ascending: !this.state.ascending
    })
  }

  getStatus(winner, currentBoardSquares, turn) {
    let status = '';

    if (winner) {
      status = `Winner: ${winner.side}`;
    }
    else if (!currentBoardSquares.includes(null)) {
      status = `Draw!`;
    }
    else {
      status = `Next player: ${turn}`;
    }

    return status;
  }

  getMoveList(history) {
    const moves = history.map((step, move) => {
      const turn = step.turn;
      const location = step.location;
      const row = Math.floor(location / 3);
      const column = location % 3;
      const styleClass = (move === this.state.viewStep) ? 'current' : '';
      const desc = move ?
        `Go to move #${move}: ${turn} (${row}, ${column})` :
        'Go to game start';
      return (
        <li key={move} className={styleClass}>
          <button className={styleClass} onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>);
    });
    const moveList = this.state.ascending ? moves : moves.reverse();
    return moveList;
  }

  render() {

    const history = this.state.history;
    const current = history[this.state.viewStep];
    const winner = calculateWinner(current.squares);
    const turn = this.state.turnX ? 'X' : 'O';
    const status = this.getStatus(winner, current.squares, turn);

    const moveList = this.getMoveList(history);
    const winningSquares = winner ? winner.squares : null;

    return (
      <div className="game">
        <div className="game-board">
          <Board
            winningSquares={winningSquares}
            squares={current.squares}
            onClick={(i) => this.fillSquare(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <Sort ascending={!this.state.ascending} onClick={() => this.sortHistory()} />
          <ol>{moveList}</ol>
        </div>
      </div>
    );
  }
}

export default Game;