function calculateWinner(squares) {

  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      let winner = {};
      winner.side = squares[a];
      winner.squares = [];
      winner.squares.push(a);
      winner.squares.push(b);
      winner.squares.push(c);

      return winner;
    }
  }

  return null;
}

export default calculateWinner;